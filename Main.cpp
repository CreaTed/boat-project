#include <iostream>
#include <string>
#include <vector>
#include "Individuo.cpp"
#include "Individuos.cpp"
#include "Lugar.cpp"
#include "Lugares.cpp"
#include "Barca.cpp"
#include "Orilla.cpp"


int main()
{
	Individual rabbit("Conejo", 'C');
	Individual lettuce("Lechuga", 'L');
	Individual robot("Robot", 'R');
	Individual fox("Zorro", 'Z');
	Boat boat("boat", 'B');

	const int numberOfIndividuosToWin = 4;

	Place leftSide("Left Corner");
	leftSide.AssignIndividual(boat);
	Place rigthSide("Rigth Corner");

	Individuals individuals;
	individuals.AssignIndividual(rabbit);
	individuals.AssignIndividual(lettuce);
	individuals.AssignIndividual(robot);
	individuals.AssignIndividual(fox);
	individuals.AssignIndividual(boat);

	Orilla leftShore("Left Shore");
	leftShore.AssignIndividual(rabbit);
	leftShore.AssignIndividual(lettuce);
	leftShore.AssignIndividual(robot);
	leftShore.AssignIndividual(fox);
	Orilla rigthShore("Right Shore");	

	Places places;
	places.AssignPlace(leftSide);
	places.AssignPlace(rigthSide);
	places.AssignPlace(leftShore);
	places.AssignPlace(rigthShore);

	

	bool gameOver = false;

	Place boatPlace("");
	Place currentPlace("");

	std::cout << "Bienvenido al juego de la barca!!" << std::endl;
	char operation;

	std::cout << "Presione la B para que la barca se mueva de un lugar a otro" << std::endl;
	std::cout << "Presione la R para que el robot salte  de/a la boat" << std::endl;
	std::cout << "Presione la Z para que el zorro salte de/a la boat" << std::endl;
	std::cout << "Presione la C para que el conejo salte de/a la boat" << std::endl;
	std::cout << "Presione la L para que la lechuga salte de/a la boat" << std::endl;
	do
	{			
		std::cin >> operation;

		if (individuals.IsAvalidPrefix(operation))
		{
			Individual individual = individuals.getIndividualByPrefix(operation);

			if (operation == 'B')
			{				
				if (boat.IndividualInBarca(robot))
				{
					boatPlace = places.WhereIsTheIndividual(boat);					
					if (boatPlace.GetName() == leftSide.GetName())
					{
						places.RemoveIndividualFromPlace(leftSide, boat);		
						places.AssignIndividualToPlace(rigthSide, boat);							
						std::cout << "La barca paso de la esquina izquierda a la esquina derecha " << std::endl;
					}
					else
					{						
						places.RemoveIndividualFromPlace(rigthSide, boat);
						places.AssignIndividualToPlace(leftSide, boat);
						std::cout << "La barca paso de la esquina derecha a la esquina izquierda " << std::endl;
					}					
				}
				else
				{
					std::cout << "La boat solo se puede mover si esta el robot" << std::endl;
					continue;
				}
			}
			else
			{
				currentPlace = places.WhereIsTheIndividual(individual);
				boatPlace = places.WhereIsTheIndividual(boat);
				if (currentPlace.GetName() == leftShore.GetName() && boatPlace.GetName() != leftSide.GetName())
				{
					std::cout << "Perdio, el " << individual.GetName() << " cayo al agua :'( " << std::endl;
					gameOver = true;
					break;
				}
				
				if (!boat.IndividualInBarca(individual))
				{
					boat.AssignIndividual(individual);
					std::cout << "El " << individual.GetName() << " salto a la boat" << std::endl;

					if (boatPlace.GetName() == leftSide.GetName())
						places.RemoveIndividualFromPlace(leftShore, individual);
					else
						places.RemoveIndividualFromPlace(rigthShore, individual);
				}
				else
				{
					boatPlace = places.WhereIsTheIndividual(boat);
					currentPlace = places.WhereIsTheIndividual(individual);
					if (boatPlace.GetName() == leftSide.GetName())
					{						
						places.AssignIndividualToPlace(leftShore, individual);
						boat.RemoveIndividual(individual);
						std::cout << "El " << individual.GetName() << " salto a " << leftShore.GetName() << std::endl;
					}
					else
					{						
						places.AssignIndividualToPlace(rigthShore, individual);
						boat.RemoveIndividual(individual);
						std::cout << "El " << individual.GetName() << " salto a " << rigthShore.GetName() << std::endl;
					}
				}
				
				if (places.NumberOfIndividuals(leftShore) == 2 || places.NumberOfIndividuals(rigthShore) == 2)
				{					
					if ((places.WhereIsTheIndividual(rabbit).GetName() != "" && places.WhereIsTheIndividual(lettuce).GetName() != "")
						&& (places.WhereIsTheIndividual(rabbit).GetName() == places.WhereIsTheIndividual(lettuce).GetName()))
					{
						std::cout << "Perdio, el conejo se comio la lechuga" << std::endl;
						gameOver = true;
						break;
					}

					if ((places.WhereIsTheIndividual(fox).GetName() != "" && places.WhereIsTheIndividual(rabbit).GetName() != "")
						&& (places.WhereIsTheIndividual(fox).GetName() == places.WhereIsTheIndividual(rabbit).GetName()))
					{
						std::cout << "Perdio, el zorro se comio el conejo " << std::endl;
						gameOver = true;
						break;
					}
				}

				if (places.NumberOfIndividuals(rigthShore) == numberOfIndividuosToWin)
				{
					std::cout << "Felicidades, gano el juego" << std::endl;
					gameOver = true;
					break;
				}
			}			
		}
		else
			std::cout << "Debe ingresar una opcion valida" << std::endl;

	} while (gameOver == false);

	system("pause");
	return 0;
}
