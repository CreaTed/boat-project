#include "Barca.hpp"
#include <vector>
#include <algorithm>

void Boat::AssignIndividual(const Individual& individual)
{
	boatIndividuals.push_back(individual);
}

unsigned int Boat::GetNumberOfMembers()
{
	return boatIndividuals.size();
}

bool Boat::IndividualInBarca(const Individual& individual)
{
	std::vector<Individual>::iterator it;

	it = std::find(boatIndividuals.begin(), boatIndividuals.end(), individual);

	if (it != boatIndividuals.end())
		return true;
	else
		return false;
}

void Boat::RemoveIndividual(const Individual& individual)
{
	boatIndividuals.erase(std::remove(boatIndividuals.begin(), boatIndividuals.end(), individual), boatIndividuals.end());
}
