#pragma once
#include "Individuo.hpp"
#include <string>
#include <vector>

class Boat : public Individual
{
public:	
	Boat(const std::string& _name, const char& _prefix) : Individual(_name, _prefix) { };
	void AssignIndividual(const Individual& individual);
	unsigned int GetNumberOfMembers();
	bool IndividualInBarca(const Individual& individual);
	void RemoveIndividual(const Individual& individual);
private:
	std::vector<Individual> boatIndividuals;
};
