#include "Individuo.hpp"
#include <utility>

std::string Individual::GetName()
{
	return name;
};

char Individual::GetPrefix()
{
	return prefix;
};

Individual::Individual(const std::string& _name, const char& _prefijo)
{
	name = _name;
	prefix = _prefijo;
}

bool operator==(const Individual& lhs, const Individual& rhs)
{
	return lhs.name == rhs.name;
}
