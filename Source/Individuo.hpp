#pragma once
#include <string>
#include <vector>

class Individual
{
public:
	std::string GetName();
	char GetPrefix();
	
	Individual(const std::string& _name, const char& _prefix);
protected:
	friend bool operator==(const Individual& lhs, const Individual& rhs);
private:
	std::string name;
	char prefix;
};
