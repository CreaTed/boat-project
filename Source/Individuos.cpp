#include "Individuos.hpp"

void Individuals::AssignIndividual(const Individual& individual)
{
	individuals.push_back(individual);
}

bool Individuals::IsAvalidPrefix(const char& _prefix)
{
	bool prefixFound = false;
	for (auto& individuo : individuals)
	{
		if (individuo.GetPrefix() == _prefix)
		{
			prefixFound = true;
			break;
		}
	}

	if (prefixFound)
		return true;
	else
		return false;
}

Individual Individuals::getIndividualByPrefix(const char& _prefix)
{
	Individual individual("", 'R');
	for (auto& individual : individuals)
	{
		if (individual.GetPrefix() == _prefix)
			return individual;
	}

	return individual;
}