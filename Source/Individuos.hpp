#include "Individuo.hpp"

class Individuals
{
public:
	void AssignIndividual(const Individual& individual);
	bool IsAvalidPrefix(const char& _prefix);
	Individual getIndividualByPrefix(const char& _prefix);
private:
	std::vector<Individual> individuals;
};