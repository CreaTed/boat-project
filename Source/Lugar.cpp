#include "Lugar.hpp"
#include <vector>
#include <algorithm>
#include <typeinfo>

void Place::AssignIndividual(const Individual& individual)
{
	individuals.push_back(individual);
}

Place::Place(const std::string& _name)
{
	name = _name;
}

std::string Place::GetName()
{
	return name;
}

bool Place::IsIndividualInPlace(const Individual& individual)
{
	std::vector<Individual>::iterator it;

	it = std::find(individuals.begin(), individuals.end(), individual);
	
	if (it != individuals.end())
		return true;
	else
		return false;
}

void Place::RemoveIndividual(const Individual& individual)
{
	individuals.erase(std::remove(individuals.begin(), individuals.end(), individual), individuals.end());
}

int Place::NumberOfIndividuals()
{
	return individuals.size();
}

bool operator==(const Place& placeA, const Place& placeB)
{
	return placeA.name == placeB.name;
}

