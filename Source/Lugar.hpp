#pragma once
#include <vector>
#include "Individuo.hpp"

class Place
{
public:
	void AssignIndividual(const Individual& individual);
	void RemoveIndividual(const Individual& individual);
	bool IsIndividualInPlace(const Individual& individual);
	int NumberOfIndividuals();
	std::string GetName();
	Place(const std::string& _name);
protected:
	friend bool operator==(const Place& placeA, const Place& placeB);
private:
	std::vector<Individual> individuals;
	std::string name;

};
