#include "Lugares.hpp"
#include "Lugar.hpp"
#include <vector>
#include <algorithm>

void Places::AssignPlace(const Place& place)
{
	places.push_back(place);
}

Place Places::WhereIsTheIndividual(const Individual& individual)
{
	Place place("");
	for (auto& place : places)
	{
		if (place.IsIndividualInPlace(individual))
			return place;
	}

	return place;
}

int Places::NumberOfIndividuals(Place& place)
{	
	return place.NumberOfIndividuals();
}

void Places::RemoveIndividualFromPlace(Place& place, const Individual& individual)
{
	std::vector<Place>::iterator it;

	it = std::find(places.begin(), places.end(), place);

	if (it != places.end())
	{
		it->RemoveIndividual(individual);
		place.RemoveIndividual(individual);
	}
}

void Places::AssignIndividualToPlace(Place& place, const Individual& individual)
{
	std::vector<Place>::iterator it;

	it = std::find(places.begin(), places.end(), place);

	if (it != places.end())
	{
		it->AssignIndividual(individual);
		place.AssignIndividual(individual);
	}
}