#pragma once
#include <vector>
#include "Individuo.hpp"

class Places
{
public:
	void AssignPlace(const Place& place);
	Place WhereIsTheIndividual(const Individual& individual);
	int NumberOfIndividuals(Place& place);			
	void RemoveIndividualFromPlace(Place& place, const Individual& individual);
	void AssignIndividualToPlace(Place& place, const Individual& individual);
private:
	std::vector<Place> places;	
};