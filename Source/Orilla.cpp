#include "Orilla.hpp"
#include <algorithm>

void Orilla::AssignIndividual(const Individual& individual)
{
	Place::AssignIndividual(individual);
}

void Orilla::removeIndividuals(const Individual& individual)
{	
	Place::RemoveIndividual(individual);
}

Orilla::Orilla(const std::string& _name) : Place(_name) { name = _name; };