#pragma once
#include "Individuo.hpp"
#include <string>
#include "Barca.hpp"
#include "Lugar.hpp"
#include <vector>

class Orilla : public Place
{
public:	
	void AssignIndividual(const Individual& individual);
	void removeIndividuals(const Individual& individual);
	Orilla(const std::string& _name);
private:	
	std::string name;
};
